![Image](https://crossref.org/img/labs/creature1.svg)

# Abstractonator

Version: 2



This game tests whether you can distinguish machine-generated content (MGC) from human-generated content (HGC). Specifically, it focuses on whether you can distinguish between machine-generated and human-written scholarly abstracts.

Click the `Play` button below to begin.

It will keep your score in the left sidebar.

To restart the game, reload the page.




