{
    "indexed": {
        "date-parts": [
            [
                2022,
                4,
                3
            ]
        ],
        "date-time": "2022-04-03T22:12:32Z",
        "timestamp": 1649023952695
    },
    "reference-count": 38,
    "publisher": "MDPI AG",
    "issue": "20",
    "license": [
        {
            "start": {
                "date-parts": [
                    [
                        2020,
                        10,
                        15
                    ]
                ],
                "date-time": "2020-10-15T00:00:00Z",
                "timestamp": 1602720000000
            },
            "content-version": "unspecified",
            "delay-in-days": 0,
            "URL": "https://creativecommons.org/licenses/by/4.0/"
        }
    ],
    "content-domain": {
        "domain": [],
        "crossmark-restriction": false
    },
    "short-container-title": [
        "Sustainability"
    ],
    "abstract": "<jats:p>Biological invasions are an ongoing threat for sustainability of ecosystems, and estimating the spread of invasive species is critical for making management decisions. Geopolitical unit-level data (GULD) are often used to estimate invasions due to their wide availability, and researchers had evaluated the abilities of multiple methods to estimate invasion with GULD. However, earlier studies were case based and only addressed limited information on the spread, thus making it inadequate to determine which method to choose to estimate invasions with GULD under various spread scenarios. Here, we conducted a simulation study to (1) evaluate performances of eight methods on estimating expansion patterns, spread rates, and spread dynamics of invasive species with GULD; (2) assess the impact of size and homogeneity of size of geopolitical unit on the estimations by studied methods; (3) evaluate the similarities of all studied methods. Additionally, we presented a concave hull boundary displacement method (Ctd_BD) and an area-based regression method (SqrtNA_R) to estimate spread with GULD. Three regions with varying sizes of counties in the United States (U.S.) were selected to conduct the simulations, and three spread scenarios and three expansion patterns were simulated. AIC, and R2 and root mean square error (RMSE) were used to evaluate the accuracy of methods on estimating expansion pattern, and overall spread rate and spread dynamics, respectively. Correlation coefficient and RMSE were used to assess the similarity of eight methods. We found Ctd_BD and area-based regression methods consistently estimated the right expansion patterns. Boundary displacement and area-based regression methods estimated highly correlated spread rates and dynamics. Distance-based regression methods provided a high accuracy on estimating overall spread rate without long-distance jump dispersal but performed poorly on estimating the spread dynamics. We recommend boundary displacement method, especially Ctd_BD, for estimating spread with GULD, whereas for spread without clear infestation boundaries, distance-based regression can be used to estimate overall spread rate and area-based regression can be used to estimate spread dynamics.</jats:p>",
    "DOI": "10.3390/su12208526",
    "type": "journal-article",
    "created": {
        "date-parts": [
            [
                2020,
                10,
                16
            ]
        ],
        "date-time": "2020-10-16T01:42:21Z",
        "timestamp": 1602812541000
    },
    "page": "8526",
    "source": "Crossref",
    "is-referenced-by-count": 0,
    "title": [
        "Estimating Invasion Dynamics with Geopolitical Unit-Level Records: The Optimal Method Depends on Irregularity and Stochasticity of Spread"
    ],
    "prefix": "10.3390",
    "volume": "12",
    "author": [
        {
            "ORCID": "http://orcid.org/0000-0001-9588-7182",
            "authenticated-orcid": false,
            "given": "Wanwan",
            "family": "Liang",
            "sequence": "first",
            "affiliation": []
        },
        {
            "given": "Liem",
            "family": "Tran",
            "sequence": "additional",
            "affiliation": []
        },
        {
            "given": "Jerome",
            "family": "Grant",
            "sequence": "additional",
            "affiliation": []
        },
        {
            "ORCID": "http://orcid.org/0000-0001-7299-6693",
            "authenticated-orcid": false,
            "given": "Vivek",
            "family": "Srivastava",
            "sequence": "additional",
            "affiliation": []
        }
    ],
    "member": "1968",
    "published-online": {
        "date-parts": [
            [
                2020,
                10,
                15
            ]
        ]
    },
    "reference": [
        {
            "key": "ref1",
            "doi-asserted-by": "publisher",
            "DOI": "10.1073/pnas.1602205113"
        },
        {
            "key": "ref2",
            "doi-asserted-by": "publisher",
            "DOI": "10.1016/j.tree.2009.03.016"
        },
        {
            "key": "ref3",
            "doi-asserted-by": "publisher",
            "DOI": "10.1093/ee/nvz014"
        },
        {
            "key": "ref4",
            "doi-asserted-by": "publisher",
            "DOI": "10.1111/j.1539-6924.2006.00707.x"
        },
        {
            "key": "ref5",
            "doi-asserted-by": "publisher",
            "DOI": "10.1046/j.1461-9563.1999.00006.x"
        },
        {
            "key": "ref6",
            "doi-asserted-by": "publisher",
            "DOI": "10.1073/pnas.98.3.1095"
        },
        {
            "key": "ref7",
            "doi-asserted-by": "publisher",
            "DOI": "10.1007/s10530-006-9039-z"
        },
        {
            "key": "ref8",
            "doi-asserted-by": "publisher",
            "DOI": "10.1111/j.1472-4642.2007.00431.x"
        },
        {
            "key": "ref9",
            "doi-asserted-by": "publisher",
            "DOI": "10.1603/0046-225X-32.2.397"
        },
        {
            "key": "ref10",
            "doi-asserted-by": "publisher",
            "DOI": "10.1111/j.1442-9993.2012.02388.x"
        },
        {
            "key": "ref11",
            "doi-asserted-by": "publisher",
            "DOI": "10.1111/1365-2745.12692"
        },
        {
            "key": "ref12",
            "doi-asserted-by": "publisher",
            "DOI": "10.1007/s10530-013-0521-0"
        },
        {
            "key": "ref13",
            "doi-asserted-by": "publisher",
            "DOI": "10.2307/2845737"
        },
        {
            "key": "ref14",
            "doi-asserted-by": "publisher",
            "DOI": "10.1111/j.1365-2699.2006.01600.x"
        },
        {
            "key": "ref15",
            "series-title": "Pest Risk Modelling and Mapping for Invasive Alien Species",
            "first-page": "131",
            "article-title": "Estimating spread rates of non-native species: The gypsy moth as a case study",
            "author": "Tobin",
            "year": "2015"
        },
        {
            "key": "ref16",
            "doi-asserted-by": "publisher",
            "DOI": "10.1111/j.1461-0248.2004.00687.x"
        },
        {
            "key": "ref17",
            "doi-asserted-by": "publisher",
            "DOI": "10.1098/rspb.2018.2294"
        },
        {
            "key": "ref18",
            "doi-asserted-by": "publisher",
            "DOI": "10.1111/ecog.01393"
        },
        {
            "key": "ref19",
            "doi-asserted-by": "publisher",
            "DOI": "10.1111/j.1600-0587.2009.06018.x"
        },
        {
            "key": "ref20",
            "doi-asserted-by": "publisher",
            "DOI": "10.1186/1297-9716-42-60"
        },
        {
            "key": "ref21",
            "series-title": "Biological Invasions: Theory and Practice",
            "author": "Shigesada",
            "year": "1997"
        },
        {
            "key": "ref22",
            "doi-asserted-by": "publisher",
            "DOI": "10.1093/biomet/38.1-2.196"
        },
        {
            "key": "ref23",
            "doi-asserted-by": "publisher",
            "DOI": "10.1086/285796"
        },
        {
            "key": "ref24",
            "doi-asserted-by": "publisher",
            "DOI": "10.2980/i1195-6860-12-3-424.1"
        },
        {
            "key": "ref25",
            "doi-asserted-by": "publisher",
            "DOI": "10.2307/2845770"
        },
        {
            "key": "ref26",
            "doi-asserted-by": "publisher",
            "DOI": "10.1111/j.1365-2699.2010.02329.x"
        },
        {
            "key": "ref27",
            "series-title": "R: A Language and Environment for Statistical Computing",
            "year": "2013"
        },
        {
            "key": "ref28",
            "unstructured": "Package \u2018rgdal\u2019: Bindings for the Geospatial Data Abstraction Libraryhttps://cran.r-project.org/web/packages/rgdal/index.html"
        },
        {
            "key": "ref29",
            "unstructured": "Package \u2018Metrics\u2019https://cran.r-project.org/web/packages/Metrics/index.html"
        },
        {
            "key": "ref30",
            "series-title": "Selected Papers of hirotugu Akaike",
            "article-title": "Information theory and an extension of the maximum likelihood principle",
            "author": "Akaike",
            "year": "1973"
        },
        {
            "key": "ref31",
            "doi-asserted-by": "publisher",
            "DOI": "10.1098/rspb.2010.0494"
        },
        {
            "key": "ref32",
            "doi-asserted-by": "publisher",
            "DOI": "10.1111/j.1366-9516.2006.00250.x"
        },
        {
            "key": "ref33",
            "doi-asserted-by": "publisher",
            "DOI": "10.1007/BF00132860"
        },
        {
            "key": "ref34",
            "doi-asserted-by": "publisher",
            "DOI": "10.1111/ddi.12303"
        },
        {
            "key": "ref35",
            "doi-asserted-by": "publisher",
            "DOI": "10.1007/s10530-008-9420-1"
        },
        {
            "key": "ref36",
            "doi-asserted-by": "publisher",
            "DOI": "10.1126/science.1124975"
        },
        {
            "key": "ref37",
            "doi-asserted-by": "publisher",
            "DOI": "10.1111/j.1366-9516.2006.00218.x"
        },
        {
            "key": "ref38",
            "doi-asserted-by": "publisher",
            "DOI": "10.1890/ES10-00192.1"
        }
    ],
    "container-title": [
        "Sustainability"
    ],
    "language": "en",
    "link": [
        {
            "URL": "https://www.mdpi.com/2071-1050/12/20/8526/pdf",
            "content-type": "unspecified",
            "content-version": "vor",
            "intended-application": "similarity-checking"
        }
    ],
    "deposited": {
        "date-parts": [
            [
                2020,
                10,
                16
            ]
        ],
        "date-time": "2020-10-16T01:56:43Z",
        "timestamp": 1602813403000
    },
    "score": 0.0,
    "resource": {
        "primary": {
            "URL": "https://www.mdpi.com/2071-1050/12/20/8526"
        }
    },
    "issued": {
        "date-parts": [
            [
                2020,
                10,
                15
            ]
        ]
    },
    "references-count": 38,
    "journal-issue": {
        "issue": "20",
        "published-online": {
            "date-parts": [
                [
                    2020,
                    10
                ]
            ]
        }
    },
    "alternative-id": [
        "su12208526"
    ],
    "URL": "http://dx.doi.org/10.3390/su12208526",
    "ISSN": [
        "2071-1050"
    ],
    "issn-type": [
        {
            "value": "2071-1050",
            "type": "electronic"
        }
    ],
    "subject": [
        "Management, Monitoring, Policy and Law",
        "Renewable Energy, Sustainability and the Environment",
        "Geography, Planning and Development"
    ],
    "published": {
        "date-parts": [
            [
                2020,
                10,
                15
            ]
        ]
    },
    "ml-generated-abstracts": {
        "id": "chatcmpl-7XwA9nXroYt4IHKwNnwfkmNwD9u1i",
        "object": "chat.completion",
        "created": 1688322757,
        "model": "gpt-4-0613",
        "choices": [
            {
                "index": 0,
                "message": {
                    "role": "assistant",
                    "content": "Understanding the patterns and dynamics of invasions, including both biological and geopolitical invasions, is essential for strategic planning and policy-making. This research investigates the most effective methods to estimate invasion dynamics using geopolitical unit-level records, taking into account the irregularity and stochasticity (randomness) of spread. The article explores different statistical models and evaluates them based on their adaptability under varying conditions of irregularity and stochastic behavior.\n\nA primary focus lies on methods that incorporate global positioning data and temporal information, such as spatio-temporal correlation structures and spread metrics. These methods allow us to map the progress of invasions over time and space. We contrast these techniques with traditional approaches based primarily on spatial coordinates and time vectors, investigating the robustness of each approach under conditions of irregular invasion patterns and random fluctuations.\n\nOur results reveal that effectiveness varies significantly depending on the level of randomness and irregularity involved. Standard approaches tend to perform well when the invasion spread is regular or semi-regular and displays low levels of stochasticity. In contrast, methods integrating spatial-temporal correlations provide more accurate estimates when invasion patterns are irregular and exhibit a high level of stochasticity.\n\nWe also discuss the implications of our findings for strategic planning, emphasizing that choosing the optimum method to estimate invasion dynamics should align with the specific circumstances of the invasion. Consequently, the implications suggest the importance of having a deep understanding of the nature of the invasion process, including its stochasticity and irregularity, to select the most reliable estimation method.\n\nLastly, we identify potential areas for further study, particularly the development of hybrid models that could combine the strengths of the different methods tested. This research contributes to the field by optimizing the analytical process, aiming to enhance strategies for managing and predicting invasions in various contexts. Our findings not only supply valuable insights for policymakers but may also be beneficial for ecology where similar dynamics can be observed. By augmenting our understanding of invasion dynamics, we can better prepare and respond to these complex phenomena across an array of sectors."
                },
                "finish_reason": "stop"
            },
            {
                "index": 1,
                "message": {
                    "role": "assistant",
                    "content": "Invasion dynamics continue to gain relevance in the scope of geopolitical research. This study focuses on evaluating methodologies for estimating these dynamics using data derived from geopolitical unit-level records. Insight into the pattern of invasion, especially as it relates to stochasticity and irregularity of spread, is of paramount importance to establish strategic focus and effective planning in geopolitical landscapes.\n\nExisting literature provides a set of methodologies for estimating invasion dynamics; however, the effectiveness and efficiency of these approaches vary significantly based on the degree of irregularity and stochasticity inherent in the invasion spread. For this reason, this study emphasizes a comprehensive comparative analysis of these methods. Four main methods were selected: spatial stratified heterogeneity, the geographical distance method, the incidence function, and the gradient of invasion speed.\n\nThe spatial stratified heterogeneity approach substantially contributes to understanding the randomness and heterogeneity in the spread pattern. The geographical distance method emphasizes the relevance of distance in the invasion process. The incidence function shows temporal changes in local environmental situations, and the gradient of invasion speed explores the vegetative propagation rate of invasive species. \n\nTo elaborate and compare the effectiveness of these methodologies, an extensive evaluation was conducted using synthetic datasets. Results were validated on real-world invasion data from different geopolitical contexts. Outcomes substantiated that no one-size-fits-all approach suffices in effectively estimating invasion dynamics. Every method showed efficacy in specific types of spread and faltered when dealing with others.\n\nFurthermore, the evaluation recognized the necessity of integrating multiple methodologies for a robust estimation of invasion dynamics. By compensating for the disadvantages of one method with the strengths of another, an integrated approach delivers more accurate and insightful estimates of invasion dynamics.\n\nThis rigorous evaluation and comparison of methodologies are expected to offer colossal benefits to researchers, policymakers, and geopolitical strategists. Understanding the invasion dynamics can significantly help in creating proactive strategies for managing invasions, thereby reducing their adverse impact on the geopolitical aspect. Future work will seek to improve these methodologies and create a more unified approach for analyzing invasion dynamics."
                },
                "finish_reason": "stop"
            },
            {
                "index": 2,
                "message": {
                    "role": "assistant",
                    "content": "Understanding the invasion dynamics is crucial to mitigating the adverse effects of invasive species on ecosystem services. Recent methodologies facilitate more accurate and precise estimations with the help of geopolitical unit-level records. However, two characteristics of invasive species dispersal, irregularity and stochasticity, may affect the choice of the optimal method. This research endeavoured to untangle the complexities around the choice of the optimal method for estimating invasion dynamics based on shortcomings associated with irregularity and stochasticity of spread.\n\nUtilizing a unique blend of simulation modelling and empirical data, this research tested three distinct yet prominent methods: the heat map method, the network-based method, and the boundary-based method. The irregularity of the spatial spread was manipulated through the alteration of the dispersion kernel's shape, whereas the stochasticity of temporal spread was controlled via the manipulation of population growth rates.\n\nResults of the research indicate that for highly irregular spread patterns, the heat map method offers the most precise estimations. Under low irregularity situations, the network-based method delivers better results. Meanwhile, the boundary-based method has been found to suffer from sizeable estimation biases, thereby making its applicability limited.\n\nRegarding stochasticity, results evidenced that when temporal spread is purely deterministic, the heat map method and network-based method offer similar precision levels. Conversely, under high levels of stochasticity, the network-based method is the superior choice over the heat map method, albeit with a marginal improvement.\n\nThe findings of this comprehensive analysis shed light on the methodological diversity for estimating invasion dynamics and underscore the need for considering the spatio-temporal characteristics of spread while deciding on the methodology. It also serves as a guideline for practitioners and decision-makers involved in invasive species management, thereby enabling them to make more informed choices.\n\nOverall, it is vital to note that the choice of methodologies for invasion dynamic estimation significantly depends on the degree of irregularity and stochasticity of spread patterns, underscoring the need for adaptive and flexible approaches in this realm. Future research should be directed towards developing hybrid models that can synergistically integrate different methods, contributing to the advancement of the discipline.\n"
                },
                "finish_reason": "stop"
            }
        ],
        "usage": {
            "prompt_tokens": 104,
            "completion_tokens": 1213,
            "total_tokens": 1317
        }
    }
}