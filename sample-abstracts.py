import json
import logging

import typer
from habanero import Crossref
from joblib import Parallel, delayed
from langdetect import detect

MAX_SAMPLE_BATCH_SIZE = 100
CPUS_TO_USE = -2  # leave a few cores alone to be polite


API_URL = "https://api.crossref.org"
UA_STR = "Sample-Abstracts"
SAMPLE_SIZE = 101
CR = Crossref()


logging.basicConfig(level="INFO", format="%(asctime)s %(message)s")
logger = logging.getLogger(__name__)


def sample(
    batch_size: int = MAX_SAMPLE_BATCH_SIZE, filter: dict = None, headers: dict = None
) -> list(dict()):
    return CR.works(sample=batch_size, filter=filter, headers=headers)["message"][
        "items"
    ]


def p_big_sample(
    sample_size: int = MAX_SAMPLE_BATCH_SIZE, filter: dict = None, headers: dict = None
) -> list(dict()):
    """
    Returns a large sample by calling batches of smaller samples in parallel
    Arguments:
        sample_size: an integer
        filter: a dictionary filter that follows habanero syntax
    Returns:
        A sample list of works items matching the filter conditions
    """
    return [
        item
        for sublist in Parallel(n_jobs=CPUS_TO_USE, prefer="threads")(
            delayed(sample)(i, filter, headers)
            for i in [
                MAX_SAMPLE_BATCH_SIZE
                for _ in range(int(sample_size / MAX_SAMPLE_BATCH_SIZE))
            ]
            + [sample_size % MAX_SAMPLE_BATCH_SIZE]
        )
        for item in sublist
    ]


def has_english_abstract(items: list) -> list(dict()):
    return [item for item in items if detect(item["abstract"]) == "en"]


def main(
    mailto: str = typer.Option(..., envvar="MAILTO"),
    sample_size: int = typer.Option(default=10),
):
    ua_headers = {"User-Agent": f"{UA_STR}; {mailto}"}
    logger.info(f"getting sample of {sample_size} records")
    sample = p_big_sample(
        sample_size=sample_size,
        filter={"has-abstract": "t", "type": "journal-article"},
        headers=ua_headers,
    )
    logger.info("selecting abstracts that are in English")
    sample = has_english_abstract(sample)
    logger.info(f"retrieved {len(sample)} records with abstracts in English")
    print(json.dumps(sample, indent=4))


if __name__ == "__main__":
    typer.run(main)
