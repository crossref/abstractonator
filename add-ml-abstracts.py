import json
import logging

import openai
import typer
import hashlib

# import pathlib
from pathlib import Path
from bs4 import BeautifulSoup


AVAILABLE_MODELS = [
    "gpt-4",
    "gpt-4-0613",
    "gpt-4-32k",
    "gpt-4-32k-0613",
    "gpt-3.5-turbo",
    "gpt-3.5-turbo-0613",
    "gpt-3.5-turbo-16k",
    "gpt-3.5-turbo-16k-0613",
]

#MODEL = "gpt-3.5-turbo"
MODEL = "gpt-4"

# MAX_SAMPLE_BATCH_SIZE = 100
# CPUS_TO_USE = -2  # leave a few cores alone to be polite


# API_URL = "https://api.crossref.org"
NUMBER_OF_ML_ABSTRACTS = 3
UA_STR = "Add-ML-Abstracts"


logging.basicConfig(level="INFO", format="%(asctime)s %(message)s")
logger = logging.getLogger(__name__)

PROMPT_TEMPLATE = open("prompt.txt", "r").read()

def doi_to_md5(doi: str = None) -> str:
    return hashlib.md5(doi.lower().encode("utf-8")).hexdigest()

def strip_markup(text):
    return BeautifulSoup(text, features="html.parser").get_text()


# def first_english_example(items):
#     return next((item for item in items if detect(item["abstract"]) == "en"), None)


def xget_ml_abstracts(title: str = None, openai_api_key: str = None) -> dict:
    openai.api_key = openai_api_key
    logger.info("generating abstracts using OpenAI")
    return openai.Completion.create(
        model="text-davinci-003",
        prompt=f'Write an abstract for a scholarly article with the title "{title}". Do not start the abstract with the phrase "This paper" :\n',
        temperature=0.7,
        max_tokens=527,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0,
        n=3,
    )

def get_ml_abstracts(title: str = None, openai_api_key: str = None, word_count=30) -> dict:
    prompt = PROMPT_TEMPLATE.format(title=title, word_count=word_count)
    
    response = openai.ChatCompletion.create(
        model=MODEL,
        messages=[
            {"role": "system", "content": "You are a university researcher."},
            {"role": "user", "content": prompt},
        ],
        temperature=1,
        n = 3,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0,
    )
    return response

# def token_count(text: str = None) -> int:
#     # https://help.openai.com/en/articles/4936856-what-are-tokens-and-how-to-count-them
#     # return round(len(text) / 4)
#     return 0 if text is None else round(len(text)/4)

def word_count(text: str = None) -> int:
    return 0 if text is None else len(text.split())

def add_ml_abstracts(sample: list = None, openai_api_key=None) -> list:
    annotated_sample = []
    for record in sample:
        title = record["title"][0]
        doi = record["DOI"]
        real_abstract = record["abstract"]
        
        #logger.info(f"{doi} / title: {title} / abstract token length {token_count(real_abstract)}")
        record["ml-generated-abstracts"] = get_ml_abstracts(
            title=title, openai_api_key=openai_api_key, word_count=word_count(real_abstract)
        )
        annotated_sample.append(record)
    return annotated_sample


def main(
    sample_file: Path = typer.Argument(..., exists=True, dir_okay=False, file_okay=True),
    results_dir: Path = typer.Argument(..., exists=True, dir_okay=True, file_okay=False),
    openai_api_key: str = typer.Option(..., envvar="OPENAI_API_KEY"),
    mailto: str = typer.Option(..., envvar="MAILTO"),
    max_size: int = typer.Option(default=None),
):
    ua_headers = {"User-Agent": f"{UA_STR}; {mailto}"}
    logger.info("loading sample")
    with open(sample_file, "r") as f:
        sample = json.load(f)

    if max_size and len(sample) > max_size:
        sample = sample[:max_size]
    
    logger.info("generating openai abstracts")
    # sample = add_ml_abstracts(sample=sample, openai_api_key=openai_api_key)
    # print(json.dumps(sample, indent=4))
    for index, record in enumerate(sample):
        title = record["title"][0]
        doi = record["DOI"]
        logger.info(f" {index + 1} of {len(sample)}: {doi} ")
        real_abstract = record["abstract"]
        record["ml-generated-abstracts"] = get_ml_abstracts(
            title=title, openai_api_key=openai_api_key, word_count=word_count(real_abstract)
        )
        open(f"{results_dir}/{doi_to_md5(doi)}.json", "w").write(json.dumps(record, indent=4))
        
    logger.info("done")


if __name__ == "__main__":
    typer.run(main)
