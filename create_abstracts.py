import json
import logging

import openai
import typer
from bs4 import BeautifulSoup
from habanero import Crossref
from joblib import Parallel, delayed
from langdetect import detect

MAX_SAMPLE_BATCH_SIZE = 100
CPUS_TO_USE = -2  # leave a few cores alone to be polite

API_URL = "https://api.crossref.org"
UA_STR = "Abstractonator"
SAMPLE_SIZE = 101
CR = Crossref()


logging.basicConfig(level="INFO", format="%(asctime)s %(message)s")
logger = logging.getLogger(__name__)


def sample(
    batch_size: int = MAX_SAMPLE_BATCH_SIZE, filter: dict = None, headers: dict = None
) -> list:
    return CR.works(sample=batch_size, filter=filter, headers=headers)["message"][
        "items"
    ]


def p_big_sample(
    sample_size: int = MAX_SAMPLE_BATCH_SIZE, filter: dict = None, headers: dict = None
):
    """
    Returns a large sample by calling batches of smaller samples in parallel
    Arguments:
        sample_size: an integer
        filter: a dictionary filter that follows habanero syntax
    Returns:
        A sample list of works items matching the filter conditions
    """
    return [
        item
        for sublist in Parallel(n_jobs=CPUS_TO_USE, prefer="threads")(
            delayed(sample)(i, filter, headers)
            for i in [
                MAX_SAMPLE_BATCH_SIZE
                for _ in range(int(sample_size / MAX_SAMPLE_BATCH_SIZE))
            ]
            + [sample_size % MAX_SAMPLE_BATCH_SIZE]
        )
        for item in sublist
    ]


def strip_markup(text):
    return BeautifulSoup(text, features="html.parser").get_text()


def first_english_example(items):
    return next((item for item in items if detect(item["abstract"]) == "en"), None)


def has_english_abstract(items):
    return [item for item in items if detect(item["abstract"]) == "en"]


def get_fake_abstracts(title: str = None, openai_api_key: str = None) -> dict:
    openai.api_key = openai_api_key
    logger.info("generating abstracts using OpenAI")
    return openai.Completion.create(
        model="text-davinci-002",
        prompt=f'Write an abstract for a scholarly article with the title "{title}". :\n',
        temperature=0.7,
        max_tokens=527,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0,
        n=3,
    )


def add_fake_abstracts(sample: list = None, openai_api_key=None) -> list:
    annotated_sample = []
    for record in sample:
        title = record["title"][0]
        doi = record["DOI"]
        logger.info(f"{doi} / title: {title}")
        record["openai_abstracts"] = get_fake_abstracts(
            title=title, openai_api_key=openai_api_key
        )
        annotated_sample.append(record)
    return annotated_sample


def main(
    openai_api_key: str = typer.Option(..., envvar="OPENAI_API_KEY"),
    mailto: str = typer.Option(..., envvar="MAILTO"),
    sample_size: int = typer.Option(default=10),
):
    ua_headers = {"User-Agent": f"{UA_STR}; {mailto}"}
    logger.info(f"getting sample of {sample_size} records")
    sample = p_big_sample(
        sample_size=sample_size,
        filter={"has-abstract": "t", "type": "journal-article"},
        headers=ua_headers,
    )
    logger.info("filtering records with where abstracts are not in English")
    sample = has_english_abstract(sample)
    logger.info(f"retrieved {len(sample)} records with abstracts in English")
    logger.info("generating openai abstracts")
    sample = add_fake_abstracts(sample=sample, openai_api_key=openai_api_key)
    print(json.dumps(sample, indent=4))
    logger.info("done")


if __name__ == "__main__":
    typer.run(main)
